//@Library('jenkinslib') _

def args = [:]
def artServer = Artifactory.server 'ART_SID'
def rtGradle = Artifactory.newGradleBuild()
def buildInfo = Artifactory.newBuildInfo()

pipeline {
  agent {
    node {
      label 'jenkins-jenkins-slave'
    }
  }

  environment {
    dockerRegistry = "52.82.40.171:8082"
    dockerRepo = 'repo15657581394-docker-release-local'
    dockerCredential = 'docker-registry-credential'
  }

  stages {
    stage('Init') {
      steps {
        container('jnlp') {
          script {
            echo "=================================================="
            echo '\tGitlab ENV'
            sh "env | grep -i '^gitlab' | sort || true"
            echo "=================================================="
            // 获取构建参数
            args.gitRepo = params.GIT_REPO
            args.gitCimmitID = ''
            args.appName = params.APP_NAME
            args.appVersion = params.APP_VERSION
            args.dockerImageExists = false
            args.forceRebuild = params.FORCE_REBUILD

            if (env.gitlabSourceRepoHttpUrl) {
              args.gitRepo = env.gitlabSourceRepoHttpUrl
            }
            if (env.gitlabBranch) {
              args.appVersion = env.gitlabBranch
            }
          }
          echo "gitRepo=#$args.gitRepo#"
          echo "appVersion=#$args.appVersion#"
        }
      }
    }

    stage('Clone') {
      steps {
        container('jnlp') {
          git url: "$args.gitRepo", branch: "$args.appVersion", credentialsId: 'gitlab-goooogs'

          gitlabCommitStatus(name: '01-Check-Image-Exists') {
            script {
              // 根据 gitlab 环境变量调整构建参数
              args.gitCimmitID = sh returnStdout: true, script: 'git rev-parse --short=8 HEAD'
              args.gitCimmitID = args.gitCimmitID.trim()
              args.dockerImageName = "${env.dockerRegistry}/${env.dockerRepo}/app/${args.appName}:${args.appVersion}-${args.gitCimmitID}"
              withCredentials([
                  usernameColonPassword(credentialsId: 'docker-registry-credential', variable: 'DOCKER_REGISTRY_USER')]) {
                args.dockerImageExists = isDockerImageExists([
                    'user'    : DOCKER_REGISTRY_USER,
                    'registry': env.dockerRegistry,
                    'repo'    : env.dockerRepo,
                    'name'    : args.appName,
                    'tag'     : "${args.appVersion}-${args.gitCimmitID}"
                ])
              }
            }
          }
          echo "dockerImageName=#${args.dockerImageName}#"
        }
      }
    }

    stage('Test') {
      when {
        anyOf {
          expression { return args.forceRebuild }
          expression { return !args.dockerImageExists }
        }
      }
      steps {
        echo "gitlabSourceRepoName=${env.gitlabSourceRepoName}"
        gitlabCommitStatus(name: '02-Test') {
          script {
            container('gradle') {
              sh 'gradle clean test'
            }
          }
        }
      }
    }

    stage('Artifactory') {
      when {
        anyOf {
          expression { return args.forceRebuild }
          expression { return !args.dockerImageExists }
        }
      }
      steps {
        gitlabCommitStatus(name: '03-Artifactory') {
          script {
            def projectVersion = sh returnStdout: true, script: 'grep currentVersion gradle.properties | cut -d= -f2'
            args.projectVersion = projectVersion.trim()
            def deployerRepo = projectVersion.contains('SNAPSHOT') ? 'repo15657581394-maven-snapshot-local' : 'repo15657581394-maven-release-local'
            def resolverRepo = projectVersion.contains('SNAPSHOT') ? 'maven-snapshot-virtual' : 'maven-release-virtual'
            println "projectVersion=#$projectVersion#"
            echo "deployerRepo=#$deployerRepo#"
            println "resolverRepo=#$resolverRepo#"

            rtGradle.tool = 'gradle-5.2.1'
            rtGradle.deployer repo: deployerRepo, server: artServer
            rtGradle.resolver repo: resolverRepo, server: artServer
            container('gradle') {
              buildInfo = rtGradle.run rootDir: '.', tasks: 'clean build artifactoryPublish'
              artServer.publishBuildInfo buildInfo
            }
          }
        }
      }
    }

    stage('Docker Image') {
      when {
        anyOf {
          expression { return args.forceRebuild }
          expression { return !args.dockerImageExists }
        }
      }
      steps {
        gitlabCommitStatus(name: '04-Docker-Image-Build-and-Push') {
          script {
            def dockerfileTemplate = '''FROM 52.82.40.171:8082/repo15657581394-docker-release-local/base/java-app:jre8
COPY ${src} ${dest}
'''
            def dockerfileContext = renderTemplate(dockerfileTemplate, [
                'src' : "sample-service/build/libs/sample-service-${args.projectVersion}.jar",
                'dest': 'app.jar'
            ])
            echo "========================="
            echo "Dockerfile"
            echo "$dockerfileContext"
            echo "========================="
            writeFile file: 'Dockerfile', text: dockerfileContext
            container('docker') {
              docker.withRegistry("http://${env.dockerRegistry}", env.dockerCredential) {
                dockerImage = docker.build "$args.dockerImageName", "-f Dockerfile ."
                echo "docker push ${dockerImage.imageName()}"
                dockerImage.push()
              }
            }
          }
        }
      }
    }

    stage('Deploy') {
      input {
        // TODO: Why variables is null?
        message "即将发布的版本: ${args.appName}:${args.appVersion}\n请选择发布环境"
        ok "发布"
        parameters {
          choice(name: 'Env', description: '发布环境', choices: ['Dev', 'Demo', 'Prod'])
        }
        submitter 'admin'
      }

      steps {
//        gitlabCommitStatus(name: "05-Deploy-${params.Env}") {
          container('kubectl') {
            // TODO: 根据 Env 切换环境
            sh "kubectl set image deployment/${args.appName} ${args.appName}=${args.dockerImageName} --record"
          }
        }
//      }
    }
  }

  post {
    failure {
      emailext(
          recipientProviders: [upstreamDevelopers(), culprits()],
          subject: '$DEFAULT_SUBJECT',
          body: '$DEFAULT_CONTENT',
          mimeType: 'text/plain',
          attachmentsPattern: '**/*.html',
          attachLog: true
      )
      updateGitlabCommitStatus(name: 'Jenkins', state: 'failed')
    }

    aborted {
      updateGitlabCommitStatus(name: 'Jenkins', state: 'canceled')
    }

    success {
      archiveArtifacts allowEmptyArchive: true, artifacts: '**/*.jar', excludes: 'gradle/**/*.jar', fingerprint: true

      emailext(
          recipientProviders: [upstreamDevelopers(), culprits()],
          subject: '$DEFAULT_SUBJECT',
          body: '$DEFAULT_CONTENT',
          mimeType: 'text/plain',
          attachmentsPattern: '**/*.html',
          attachLog: false
      )
      updateGitlabCommitStatus(name: 'Jenkins', state: 'success')
    }
  }
}

import groovy.text.StreamingTemplateEngine

def renderTemplate(input, variables) {
  def engine = new StreamingTemplateEngine()
  return engine.createTemplate(input).make(variables).toString()
}

def isDockerImageExists(Map map) {
//  http://52.82.40.171:8082/artifactory/repo15657581394-docker-release-local/app/sample-service/0.1
  ret = sh returnStatus: true, script: "curl -f -sIL -u \"${map.get('user')}\" \"http://${map.get('registry')}/artifactory/${map.get('repo')}/app/${map.get('name')}/${map.get('tag')}/\" > /dev/null"
  return ret == 0
}

