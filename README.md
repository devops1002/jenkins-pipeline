All project's Jenkins Pipeline code
===
独立于项目代码之外的 Jenkins Pipeline，支持所有项目的 CI/CD，Dockerfile、构建过程等都由运维来控制。
- 优点：开发不需要了解 Dockerfile，不需要关心 Pipeline 语法、配置
- 缺点：运维需要了解各种应用的构建方式等信息

# 依赖
- 基础镜像
    - jnlp: `jenkins/inbound-agent:4.3-4`
    - gradle: `52.82.40.171:8082/repo15657581394-docker-release-local/gradle:5.2.1-jdk8-alpine`
    - docker: `52.82.40.171:8082/repo15657581394-docker-release-local/docker-cli`
    - kubectl: `52.82.40.171:8082/repo15657581394-docker-release-local/kubectl:1.17`

# Pipeline 介绍
## 功能概述
- 自动: 由 Gitlab 触发 Jenkins 中的同名 Job
    - 自动构建
        - 有新的提交
        - 有 `PR` 被合并
    - 自动测试
    - 产物管理: JFrog
    - 自动发布
    - 邮件通知
- 手动: 手动发布指定版本
    - 指定版本: `tag`、`branch`、`commit id`
    - 回滚功能
- Elastic Cloud 套件对应用性能进行监控、日志方案等

## Stage 逻辑说明
- Init: 初始化
    - 初始化构建参数
        - 包括 repo 地址、应用名称、要构建的分支等
        - 支持 Gitlab 自动触发，也支持手动发布指定版本
- Clone: 拉取应用代码
- Test: 运行 TestCase
- Artifactory: 处理 Artifacts、buildInfo 等（JFrog）
    - build、artifactoryPublish
    - publish build info
- Docker Image: build 可以发布的镜像（JFrog）
    - 根据 Dockerfile 模板渲染出 Dockerfile
    - 构建包含运行环境的 Docker 镜像
    - 推送镜像到 Docker Registry（Jfrog）
- Deploy: 发布到 EKS 集群
    - ~~input 等待选择要发布的环境~~ -> TODO: 切换上下文
    - `kubectl set image`
- 邮件通知相关人员
    - 本次构建相关的提交着
    - 自上次失败构建以来的提交者

## 使用到的工具链
- Gitlab 作为核心，围绕需求管理，通过 push、merge 等来推动 Jenkins 流水线的运转
- Jenkins 作为流水线的运行中心
- JFrog 作为构建产物及构建信息的存储仓库
    - docker registry
    - maven repo
        - snapshot
        - release
    - build info
- AWS 的 EKS 作为应用的部署环境
- Elastic Cloud 套件
    - Elastic Agent 收集系统 metrics 及应用日志
    - Elastic APM 应用性能检测
    - Kibana 日志查询及报表展示

# Demo 演示
- Jenkins 地址: [Jenkins](http://a5b4113560dce4655a703e41f574db58-1903955992.cn-northwest-1.elb.amazonaws.com.cn:10000/)

- 演示步骤
1. 在 [Gitlab](https://gitlab.com/goooogs/spring-boot-example/-/boards) 创建需求
1. 根据需求创建 branch
1. 开发把分支取下来，在本地更新代码
1. 开发推送更新，将自动触发 Jenkins Pipeline
1. 创建 PR，大家 review
1. 合并 PR 到 master 分支，将自动触发 Jenkins Pipeline
1. JFrog 查看 Artifactory
1. [Elastic Cloud](https://cloud.elastic.co/deployments/975adf120cd546c3ad6bf408736a1839) 查看 APM、日志等

- 部分截图

    图一：Jenkins Pipeline 预览
    ![jenkins-pipeline-overview](docs/images/01-jenkins-pipeline-overview.png)
    
    图二：Gitlab 需求管理看板
    ![gitlab-issues-boards](docs/images/02-gitlab-issues-boards.png)
    
    图三：Gitlab 已合并的需求
    ![gitlab-issues-merged](docs/images/03-gitlab-issues-merged.png)
    
    图四：Gitlab Pipeline 预览
    ![gitlab-pipeline-overview](docs/images/04-gitlab-pipeline-overview.png)
    
    图五：Gitlab Pipeline Job 详情
    ![gitlab-pipeline-jobs](docs/images/05-gitlab-pipeline-jobs.png)
    
    图六：JFrog Artifacts 仓库预览
    ![jfrog-artifacts](docs/images/06-jfrog-artifacts.png)
    
    图七：JFrog buildInfo 预览 1
    ![jfrog-builds](docs/images/07-jfrog-builds.png)
    
    图八：JFrog buildInfo 预览 2
    ![jfrog-builds-02](docs/images/08-jfrog-builds-02.png)
    
    图九：JFrog buildInfo 预览 3
    ![jfrog-builds-03](docs/images/09-jfrog-builds-03.png)
    
    Elastic Cloud 相关的预览参见 [spring-boot-example](https://gitlab.com/goooogs/spring-boot-example.git)

# 问题
* [ ] gitlabCommitStatus 无效
    - 在与 Jenkins 共享库一起使用时无效
    - 使用 Pipeline script from SCM 时无效
* [ ] input 的 message 属性无法使用变量，显示为 null
* [ ] 一次 merge 会触发两次 Jenkins Pipeline (push 和 merge)

# TODO
- [ ] Pipeline 逻辑优化
    - [ ] develop 分支在 CI 成功后直接发布
    - [ ] release 分支在 CI 成功后直接发布
    - [ ] 从 master 分支打 tag，需要人工介入点击确认后发布
    - [ ] 其他分支只执行 CI
- [ ] JUint
- [ ] SonarQube
